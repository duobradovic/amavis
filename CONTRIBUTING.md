# Branching model

Amavis uses a modified version of the [Gitlab
Flow](https://docs.gitlab.com/ce/workflow/gitlab_flow.html) branching
model. What that means for you practically is outlined below.

# Contributing a bugfix or feature

When you want to contribute a feature or bugfix to be merged into
Amavis, please make your changes on a feature branch based on master
and open a merge request to merge your feature branch back into
master. So roughly, follow the following steps:

 1. Fork the Amavis repository into your own namespace.
 2. Clone your forked repository to your local machines and checkout
    the master branch.
 3. Create a new feature branch based on master for your changes with
    a descriptive name.
 4. Make your changes.
 5. Test your changes and make sure they do what you want.
 6. Push you new feature branch to your forked Gitlab repository
 7. Open a merge request on the Amavis repository to merge your
    feature branch into the master branch of the official Amavis
    repository

After your merge request has been opened, we will review it and either
approve it to be merged or ask you to make further modifications to
it. If changes were made on the master branch since you started your
feature branch, we might also ask you to rebase your branch onto the
current master or merge the current master branch into your feature
branch.

If your merge request contains changes that break
backwards-compatibility, please mention this in your merge
request.

## Review process

When reviewing your merge request, we will try to make sure that it
meets our [merge request
criteria](https://gitlab.com/amavis/amavis/wikis/Merge-request-criteria),
so please try to make sure that your code actually meets these
criteria, before opening your merge request.
