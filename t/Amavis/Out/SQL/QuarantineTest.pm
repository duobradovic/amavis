# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::Out::SQL::QuarantineTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::Out::SQL::Quarantine' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class;
}

sub empty : Tests(0) {
}

1;
