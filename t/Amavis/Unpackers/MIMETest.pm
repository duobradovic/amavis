# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::Unpackers::MIMETest;

use IO::Scalar;
use MIME::Entity;
use MIME::Parser;
use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::Unpackers::MIME' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class;
}

sub ambiguous : Tests(3) {
  my $check = \&{'Amavis::Unpackers::MIME::ambiguous_content'};

  my $entity = MIME::Entity->build(
    Type    => 'multipart/mixed',
    From    => 'sender@example.com',
    To      => 'recip@example.com',
    Subject => 'Hello world',
  );

  $entity->attach(Data => 'E=mc²');
  $entity->attach(Data => 'a²+b²=c²');
  $entity->attach(Data => 'E=ma²+mb²');

  ok(!$check->($entity), 'normal MIME entity is not ambiguous');

  my $DATA = join '', <DATA>;

  $entity = MIME::Parser->new->parse(IO::Scalar->new(\$DATA));
  ok($check->($entity), 'constructed MIME entity is ambiguous');

  $entity = MIME::Parser->new->parse_data($DATA);
  ok($check->($entity), 'constructed MIME entity is ambiguous');
}

1;

__DATA__
MIME-Version: 1.0
Subject: multiple_boundary
Content-Type: multipart/mixed; boundary=bar; one=two; boundary=foo

--foo
Content-type: text/plain

Email with an attachment.
--bar
Content-type: application/octet-stream; name=name
Content-Disposition: attachment; filename=name
Content-Transfer-Encoding: 7bit

This is sparta
--bar--
--foo--
