# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::DB::SNMPTest;
use Test::Most;
use base 'Test::Class';

use File::Temp;

use Amavis::DB;

sub class { 'Amavis::DB::SNMP' }

our $tempdir;
our $db_env;

sub startup : Tests(startup => 3) {
  my $test = shift;
  use_ok $test->class;
  chdir('/');
  $tempdir = File::Temp->newdir('test-directory-tmp-XXXXX', CLEANUP => 1, TMPDIR => 1);
  $Amavis::Conf::db_home = $tempdir->dirname;
  $Amavis::Conf::daemon_chroot_dir = '';
  Amavis::DB::init(0, 0);
  $db_env = Amavis::DB->new;
  ok $db_env, 'db env created';
}

sub constructor : Tests(1) {
  my $test = shift;
  ok $test->class->new($db_env), 'Constructor succeeded';
}

sub after : Test(teardown => 1) {
    $tempdir = undef;
    ok ! -d $Amavis::Conf::db_home, 'temporary db_home removed';
}

1;
