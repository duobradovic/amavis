# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::DBTest;

use Test::Most;
use base 'Test::Class';

use File::Temp;

no warnings 'once';

sub class { 'Amavis::DB' }

our $tempdir;
our $db_env;

sub startup : Tests(startup => 1) {
  my $class = shift->class;
  use_ok $class;
  chdir('/');
  $tempdir = File::Temp->newdir('test-directory-tmp-XXXXX', CLEANUP => 1, TMPDIR => 1);
  $Amavis::Conf::db_home = $tempdir->dirname;
  $Amavis::Conf::daemon_chroot_dir = '';
}

sub init : Tests(3) {
  my $class = shift->class;
  ok eval { $class->can('init')->(0, 1); 1 };
  is $@, '', 'no init error';
  $db_env = $class->new;
  ok $db_env, 'db env created';
}

sub after : Test(teardown => 1) {
    $tempdir = undef;
    ok ! -d $Amavis::Conf::db_home, 'temporary db_home removed';
}

1;
