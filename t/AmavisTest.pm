# SPDX-License-Identifier: GPL-2.0-or-later

package AmavisTest;

use v5.20;
use Test::MockModule;
use Test::Most;
use base 'Test::Class';

package Local {
    sub new {
        my $self = bless {}, shift;
        for my $var (@_) {
            push @{$self->{vars}}, [$var,  $$var ] if ref $var eq 'SCALAR';
            push @{$self->{vars}}, [$var, [@$var]] if ref $var eq 'ARRAY';
            push @{$self->{vars}}, [$var, {%$var}] if ref $var eq 'HASH';
        }
        return $self;
    }
    sub DESTROY {
        my $self = shift;
        for my $var (@{$self->{vars}}) {
            my ($ref, $value) = @$var;
            $$ref =  $value if ref $ref eq 'SCALAR';
            @$ref = @$value if ref $ref eq 'ARRAY';
            %$ref = %$value if ref $ref eq 'HASH';
        }
    }
}

sub new_policy_bank ($@) {
    my ($name, @args) = @_;
    # Supplemented in after_chroot_init
    $Amavis::Conf::policy_bank{$name} = {
        policy_bank_name => $name,
        policy_bank_path => $name,
        @args
    };
    return $name;
}

sub class { 'Amavis' }

sub startup : Tests(startup => 1) {
    my $test = shift;
    use_ok $test->class;
}

sub init_builtin_macros : Tests(3) {
    my $test = shift;

    use_ok $test->class;
    use_ok 'Amavis::In::Message';

    local $Amavis::MSGINFO = Amavis::In::Message->new;
    $Amavis::MSGINFO->body_digest('some digest');

    Amavis::init_builtin_macros();
    is $Amavis::builtins{b}->(), 'some digest';
}

sub load_policy_bank : Tests {
    my $test = shift;

    use_ok $test->class;

    # Use own scope guard for variables. The downside of using `local $var`
    # is that a "reference relationship" is not "preserved", i.e.
    ## our $log_templ   = 'default log template';
    ## our %policy_bank = (log_templ => \$log_templ);
    ## {
    ##   local $log_templ = 'custom log template';
    ##   say ${$policy_bank{log_templ}}; # 'default log template'
    ## }
    ## say ${$policy_bank{log_templ}}; # 'default log template'
    #
    my $local = Local->new(
        \$Amavis::Conf::log_templ,
        \%Amavis::Conf::current_policy_bank,
        \%Amavis::Conf::policy_bank,
        \@Amavis::Conf::av_scanners,
    );

    $Amavis::Conf::log_templ = 'log_templ';
    @Amavis::Conf::av_scanners = ('av_scanner');

    my @log;
    my $mock = Test::MockModule->new($test->class)->redefine(
        do_log => sub ($$;@) {
            push @log, sprintf $_[1], @_[2..$#_];
        }
    );

    subtest 'base policy bank' => sub {
        # from post_accept_hook
        Amavis::load_policy_bank('');
        ok $log[$#log], 'loaded base policy bank';
        is $Amavis::Conf::current_policy_bank{policy_bank_name}, '';
        is $Amavis::Conf::current_policy_bank{policy_bank_path}, '';
        is ${$Amavis::Conf::current_policy_bank{log_templ}}, 'log_templ';
        is_deeply $Amavis::Conf::current_policy_bank{av_scanners}, ['av_scanner'];
        push @Amavis::Conf::av_scanners, 'av_scanner2';
        is_deeply $Amavis::Conf::current_policy_bank{av_scanners}, ['av_scanner', 'av_scanner2'];
    };

    subtest 'first policy bank' => sub {
        Amavis::load_policy_bank(new_policy_bank('test', log_templ => 'template'));
        is $Amavis::Conf::current_policy_bank{policy_bank_name}, 'test';
        is $Amavis::Conf::current_policy_bank{policy_bank_path}, 'test';
        is $Amavis::Conf::current_policy_bank{log_templ}, 'template';
        is_deeply $Amavis::Conf::current_policy_bank{av_scanners}, ['av_scanner', 'av_scanner2'];
    };

    subtest 'second policy bank' => sub {
        Amavis::load_policy_bank(new_policy_bank('subtest', log_templ => 'other_templ', av_scanners => ['other av_scanner']));
        is $Amavis::Conf::current_policy_bank{policy_bank_name}, 'subtest';
        is $Amavis::Conf::current_policy_bank{policy_bank_path}, 'test/subtest';
        is $Amavis::Conf::current_policy_bank{log_templ}, 'other_templ';
        is_deeply $Amavis::Conf::current_policy_bank{av_scanners}, ['other av_scanner'];
    };

    subtest 'ignoring policy bank' => sub {
        Amavis::load_policy_bank('nonexistent');
        is $log[$#log], 'policy bank "nonexistent" does not exist, ignored';

        Amavis::load_policy_bank('subtest');
        is $log[$#log], 'policy bank "subtest" just loaded, ignored';
    };
}

1;
